# saml-passthrough
This program is intended to run behind the Apache mod_auth_mellon module.
It receives ADFS user information from Apache over FastCGI, then
passes it back to Keycloak (which is acting as a SAML SP).

## Create a new keypair
```sh
openssl req -newkey rsa:2048 -nodes -keyout idp.key -x509 -out idp.crt -days 3680 -subj '/CN=SAML Passthrough/O=Computer Science Club'
```
Make sure to renew the cert in ten years.

## Apache config
Add the following snippet to /etc/apache2/sites-real/csc (and make sure mod_proxy_fcgi is enabled):
```
<Location /keycloak/saml/ >
  SetHandler "proxy:unix:/run/saml-passthrough/server.sock|fcgi://localhost"
</Location>
<Location /keycloak/saml/sso >
  AuthType Mellon
  MellonEnable auth
  Require valid-user
</Location>
```
