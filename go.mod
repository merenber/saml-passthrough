module git.csclub.uwaterloo.ca/merenber/saml-passthrough

go 1.15

require (
	github.com/crewjam/saml v0.4.6
	github.com/mattermost/xml-roundtrip-validator v0.1.0
)
